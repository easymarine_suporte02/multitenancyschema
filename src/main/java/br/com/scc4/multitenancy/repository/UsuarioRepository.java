package br.com.scc4.multitenancy.repository;

import br.com.scc4.multitenancy.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
}
