package br.com.scc4.multitenancy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * API para servir de exemplo ao iniciar a configuracao de projetos que utilizam
 * a aquitetura Multitenancy com schemas separados
 *
 * @author Felipe Corrêa
 */

@SpringBootApplication
public class MultitenancyApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultitenancyApplication.class, args);
	}

}
