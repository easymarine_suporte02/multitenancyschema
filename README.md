MultiTenancy por Schema

* API para servir de exemplo ao iniciar a configuração de projetos que utilizam a arquitetura Multitenancy com schemas.
* Para uma versão futura a criação dos schemas e tabelas será de responsabilidade do Flyway. Enquanto isso executar script abaixo:

/* Inicio do script criacacao dos databases e tabelas

CREATE DATABASE datasource1;
USE datasource1;

CREATE TABLE usuario (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE DATABASE datasource2;
USE datasource2;

CREATE TABLE usuario (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE DATABASE datasource3;
USE datasource3;

CREATE TABLE usuario (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Fim do script de criacao dos databases e tabelas